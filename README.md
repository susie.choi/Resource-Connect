The vision of the program is as follows: 
Immigrants and refugees may send requests for various needs (e.g. canned food, 
bread, clothing), and all they need to provide is an email address at which they 
may be reached. 
Individuals who have goods to donate may see if need for certain items exists.
If able to fulfill a request, a patron can provide an email address. 
A third-party (the system over-watcher) can send an email to the patron, 
arrange to pick up the donated item, and bring it to the immigrant/refugee/
recipient. 